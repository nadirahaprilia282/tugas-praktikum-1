package com.nadirahaprillia_10191063.praktikum1.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.nadirahaprillia_10191063.praktikum1.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}